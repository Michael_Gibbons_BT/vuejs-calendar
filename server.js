/* jshint esversion:6*/
require('dotenv').config({ silent: true });

const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const http = require('http');
const moment = require('moment-timezone');
moment.tz.setDefault('UTC');
const serialize= require('serialize-javascript');

app.use('/public', express.static(path.join(__dirname, 'public')));

let events=	[
		{description: 'Random Event 1', date:moment('2018-12-01','YYYY-MM-DD')},
		{description: 'Random Event 2', date:moment('2018-12-02','YYYY-MM-DD')},
		{description: 'Random Event 3', date:moment('2018-12-03','YYYY-MM-DD')},
	];


let renderer;
app.get('/', (req, res) => {
  let template = fs.readFileSync(path.resolve('./index.html'), 'utf-8');
  let contentMarker='<!--APP-->';
  if(renderer){
	  renderer.renderToString({ events },(err,html)=>{
	  	if(err){
	  		console.log(err);
	  	}else{
	  		res.send(template.replace(contentMarker,`<script>var __INITIAL_STATE__=${serialize(events)}</script>\n${html}`));
	  	}
	  });
  }else{
  	res.send('<p>Awaiting Compilation..</p><script src="/reload/reload.js"></script');
  }



});


app.use(require('body-parser').json());
app.post('/add_event',(req,res)=>{
	events.push({
		description:req.body.description,
		date: moment(req.body.date),
	});
	res.sendStatus(200);
});

const server = http.createServer(app);

if (process.env.NODE_ENV === 'development') {
  const reload = require('reload');
  const reloadServer = reload(server, app);
  require('./webpack-dev-middleware').init(app);
  require('./webpack-server-compiler').init(function(bundle){
  	let needsReload=(renderer===undefined);
  	renderer=require('vue-server-renderer').createBundleRenderer(bundle);
  	if(needsReload){
  		reloadServer.reload();
  	}
  });
}

server.listen(process.env.PORT, function () {
  console.log(`Example app listening on port ${process.env.PORT}!`);
  if (process.env.NODE_ENV === 'development') {
    require("open")(`http://localhost:${process.env.PORT}`);
  }
});

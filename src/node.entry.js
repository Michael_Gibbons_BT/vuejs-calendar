/*jshint esversion:6*/
import VueCalendar from './entry.js';

export default function(context){
	return VueCalendar(context.events);
}